<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/10/15
 * Time: 12:06 AM
 */

namespace DB1;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class Bootstrap
{
	/**
	 * @var Bootstrap
	 */
	private static $instance = null;

	/**
	 * @var EntityManager
	 */
	private $entityManager = null;

	/**
	 * @var FlashMessages
	 */
	private $flashMessages = null;

	/**
	 * Bootstrap constructor.
	 *
	 * @param bool $isDevMode
	 * @throws \Doctrine\ORM\ORMException
	 */
	private function __construct($isDevMode = false)
	{
		$paths = array(realpath(__DIR__ . '/Entity'));
		$appConfig = include __DIR__ . '/../../config/application.config.php';

		$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
		$entityManager = EntityManager::create($appConfig['db'], $config);

		$this->entityManager = $entityManager;
	}

	public static function getInstance()
	{
		if(null === self::$instance){
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Get the current EntityManager
	 *
	 * @return EntityManager
	 * @throws \Exception
	 */
	public function getEntityManager()
	{
		if(null === $this->entityManager){
			throw new \Exception("EntityManager is not configured yet");
		}

		return $this->entityManager;
	}

	/**
	 * Verifica se a requisição atual foi feita usando o método post
	 * @return bool
	 */
	public function isPost()
	{
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}

	/**
	 * Recupera um valor da superglobal $_POST. Caso
	 * a chave não esteja definida, pode-se atribuir
	 * um valor default utilizando o parâmetro $default
	 *
	 * @param string $name
	 * @param mixed $default
	 * @return mixed
	 */
	public function getPost($name, $default = null)
	{
		if(isset($_POST[$name])){
			return $_POST[$name];
		}

		return $default;
	}

	public function flashMessages()
	{
		if(null === $this->flashMessages){
			$this->flashMessages = new FlashMessages();
		}
		return $this->flashMessages;
	}
}