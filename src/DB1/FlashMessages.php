<?php
/**
 * Created by PhpStorm.
 * User: rafaellabegalini
 * Date: 8/10/15
 * Time: 12:46 AM
 */

namespace DB1;

class FlashMessages
{
	public function __construct()
	{
		//inicia a sessão
		session_start();

		if(!isset($_SESSION['messages'])){
			$_SESSION['messages'] = array();
		}
	}

	/**
	 * Adiciona uma mensagem de sucesso
	 *
	 * @param string $message
	 * @return FlashMessages
	 */
	public function addSuccessMessage($message)
	{
		$this->addMessage('success', $message);
		return $this;
	}

	/**
	 * Adiciona uma mensagem de erro
	 *
	 * @param string $message
	 * @return FlashMessages
	 */
	public function addErrorMessage($message)
	{
		$this->addMessage('error', $message);
		return $this;
	}

	private function addMessage($namespace, $message)
	{
		if(!isset($_SESSION['messages'][$namespace])){
			$_SESSION['messages'][$namespace] = array();
		}

		$_SESSION['messages'][$namespace][] = $message;
	}

	private function getMessages($namespace)
	{
		if(!isset($_SESSION['messages'][$namespace])){
			return array();
		}

		$messages = $_SESSION['messages'][$namespace];
		unset($_SESSION['messages'][$namespace]);

		return $messages;
	}

	public function render()
	{
		$templates = array(
			'success' => '<div class="alert alert-success">%s</div>',
			'error' => '<div class="alert alert-danger">%s</div>'
		);

		$namespaces = array('success', 'error');
		$messages = array();

		foreach($namespaces as $namespace) {
			foreach($this->getMessages($namespace) as $message){
				$messages[] = sprintf($templates[$namespace], $message);
			}
		}

		return implode(PHP_EOL, $messages);
	}
}