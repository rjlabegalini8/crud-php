CRUD PHP
========

Implementação do CRUD referente ao exercício 9 do teste para o processo seletivo da DB1 Global Software

Instalação
==========

Baixar o composer do site [getcomposer.org](http://getcomposer.org) ou via terminal usando os 
seguintes comandos:
	
	curl -s https://getcomposer.org/installer | php

Após baixar o composer, basta instalar as dependências do projeto.

	php composer.phar install
	
Banco de Dados
==============

Importar o arquivo `db1test.sql` para o MySql. Certifique-se de que o banco de dados `db1test` exista antes
de importar o arquivo.